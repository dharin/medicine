class Medicine < ActiveRecord::Base
  attr_accessible :code_for_placing_order, :name, :ordered_qty
  validates :code_for_placing_order, :presence =>true
  validates :name,:presence =>true
  validates :ordered_qty,:presence =>true
end
