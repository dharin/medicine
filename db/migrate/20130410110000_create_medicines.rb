class CreateMedicines < ActiveRecord::Migration
  def change
    create_table :medicines do |t|
      t.string :name
      t.string :code_for_placing_order
      t.integer :ordered_qty

      t.timestamps
    end
  end
end
